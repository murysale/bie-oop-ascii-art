import Models.Pixel.{ASCIIPixel, GreyScalePixel}
import org.scalatest.flatspec.AnyFlatSpec


class ASCIIPixelTest extends AnyFlatSpec  {
  "ASCIIPixel" should "store a Char value" in {
    val dummyPixel = ASCIIPixel(0)
    val dummyPixel2 = ASCIIPixel(1)

    assert(dummyPixel.getValue === 0)
    assert(dummyPixel2.getValue === 1)

  }
  it should "throw an exception if limits exceeded" in {
    assertThrows[IllegalArgumentException] {
      val dummyPixel = ASCIIPixel(Char.MaxValue)
      val dummyPixel3 = ASCIIPixel(256)
    }
  }
}