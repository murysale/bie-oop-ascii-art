import Models.PixelGrid
import Models.Pixel.GreyScalePixel

import org.scalatest.flatspec.AnyFlatSpec


class GreyScalePixelTest extends AnyFlatSpec  {
  "GreyScalePixel" should "store grey scale value" in {
    val dummyPixel = GreyScalePixel(0)
    val dummyPixel2 = GreyScalePixel(1)

    assert(dummyPixel.scale === 0)
    assert(dummyPixel2.scale === 1)

  }
  it should "throw an exception if limits exceeded" in {
    assertThrows[IllegalArgumentException] {
      val dummyPixel = GreyScalePixel(Int.MaxValue)
      val dummyPixel2 = GreyScalePixel(Int.MinValue)
      val dummyPixel3 = GreyScalePixel(256)
      val dummyPixel4 = GreyScalePixel(-1)
    }
  }
}