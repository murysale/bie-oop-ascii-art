package Models.Pixel

import org.scalatest.flatspec.AnyFlatSpec

class RGBAPixelTest extends AnyFlatSpec  {
  "RGBAPixel" should "store rgba value as Int" in {
    val dummyPixel = RGBAPixel(0)

    assert(dummyPixel.red() === 0)
    assert(dummyPixel.green() === 0)
    assert(dummyPixel.blue() === 0)
    assert(dummyPixel.alpha() === 0)

    val dummyPixel2 = RGBAPixel(-1)

    assert(dummyPixel2.red() === 255)
    assert(dummyPixel2.green() === 255)
    assert(dummyPixel2.blue() === 255)
    assert(dummyPixel2.alpha() === 255)

    val dummyPixel3 = RGBAPixel(279642079)

    assert(dummyPixel3.red() === 170)
    assert(dummyPixel3.green() === 255)
    assert(dummyPixel3.blue() === 223)
    assert(dummyPixel3.alpha() === 16)
  }
}
