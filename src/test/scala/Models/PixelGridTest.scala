package Models
import Models.Pixel.GreyScalePixel
import org.scalatest.flatspec.AnyFlatSpec


class PixelGridTest extends AnyFlatSpec  {
  "PixelGrid" should "return height and width" in {
    val dummyPixel = GreyScalePixel(0)
    val pixels = Seq(Seq(dummyPixel, dummyPixel, dummyPixel), Seq(dummyPixel, dummyPixel, dummyPixel))
    val pg3x2 = new PixelGrid[GreyScalePixel](pixels)

    assert(pg3x2.getHeight === 2)
    assert(pg3x2.getWidth === 3)

    val pixels2 = Seq.empty[Seq[GreyScalePixel]]
    val pg0x0 = new PixelGrid[GreyScalePixel](pixels2)

    assert(pg0x0.getHeight === 0)
    assert(pg0x0.getWidth === 0)
  }
}