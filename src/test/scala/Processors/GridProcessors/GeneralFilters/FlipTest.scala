package Processors.GridProcessors.GeneralFilters

import MediaLoaders.ImageLoaders.RandomImageLoader
import Models.Pixel.RGBAPixel
import Models.PixelGrid
import org.scalatest.flatspec.AnyFlatSpec

class FlipTest extends AnyFlatSpec {

  "Flip(flip_x=false, flip_y=true)" should "flip a PixelGrid vertically" in {
    val originalGrid: PixelGrid[RGBAPixel] = new RandomImageLoader().load()
    val newGrid = new Flip[RGBAPixel](false, true).apply(originalGrid)
    assert(newGrid.getHeight == originalGrid.getHeight && newGrid.getWidth == originalGrid.getWidth)
    for(x <- 0 until originalGrid.getWidth) {
      for(y <- 0 until originalGrid.getHeight) {
        assert(originalGrid.image(y)(x) == newGrid.image(newGrid.getHeight - 1 - y)(x))
      }
    }
  }

  "Flip(flip_x=true, flip_y=false)" should "flip a PixelGrid horizontally" in {
    val originalGrid: PixelGrid[RGBAPixel] = new RandomImageLoader().load()
    val newGrid = new Flip[RGBAPixel](true, false).apply(originalGrid)
    assert(newGrid.getHeight == originalGrid.getHeight && newGrid.getWidth == originalGrid.getWidth)
    for (x <- 0 until originalGrid.getWidth) {
      for (y <- 0 until originalGrid.getHeight) {
        assert(originalGrid.image(y)(x) == newGrid.image(y)(newGrid.getWidth - 1 - x))
      }
    }
  }

  "Flip(flip_x=true, flip_y=true)" should "flip a PixelGrid horizontally and vertically" in {
    val originalGrid: PixelGrid[RGBAPixel] = new RandomImageLoader().load()
    val newGrid = new Flip[RGBAPixel](true, true).apply(originalGrid)
    assert(newGrid.getHeight == originalGrid.getHeight && newGrid.getWidth == originalGrid.getWidth)
    for (x <- 0 until originalGrid.getWidth) {
      for (y <- 0 until originalGrid.getHeight) {
        assert(originalGrid.image(y)(x) == newGrid.image(newGrid.getHeight - 1 - y)(newGrid.getWidth - 1 - x))
      }
    }
  }

  "Flip(flip_x=false, flip_y=false)" should "do nothing" in {
    val originalGrid: PixelGrid[RGBAPixel] = new RandomImageLoader().load()
    val newGrid = new Flip[RGBAPixel](false, false).apply(originalGrid)
    assert(newGrid.getHeight == originalGrid.getHeight && newGrid.getWidth == originalGrid.getWidth)
    for (x <- 0 until originalGrid.getWidth) {
      for (y <- 0 until originalGrid.getHeight) {
        assert(originalGrid.image(y)(x) == newGrid.image(y)(x))
      }
    }
  }
}
