package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import org.scalatest.flatspec.AnyFlatSpec

class FontAspectRatioTest extends AnyFlatSpec {
  "FontAspectRatio(X: 1, Y: 2) " should "should change the size to 4x2" in {
    val columns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2))
    val resultColumns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2))
    var grid = Seq.empty[Seq[GreyScalePixel]]

    grid = grid :+ columns
    grid = grid :+ columns
    grid = grid :+ columns
    grid = grid :+ columns

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val result = new FontAspectRatio(1, 2).apply(originalGrid)

    var resultGrid = Seq.empty[Seq[GreyScalePixel]]
    resultGrid = resultGrid :+ resultColumns
    resultGrid = resultGrid :+ resultColumns

    val destGrid = new PixelGrid[GreyScalePixel](resultGrid)

    assert(result.compare(destGrid))

  }

  "FontAspectRatio(X: 2, Y: 3) " should "should change the size to 4x2" in {
    val columns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2))
    val resultColumns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2))
    var grid = Seq.empty[Seq[GreyScalePixel]]

    grid = grid :+ columns
    grid = grid :+ columns
    grid = grid :+ columns
    grid = grid :+ columns

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val result = new FontAspectRatio(2, 3).apply(originalGrid)

    var resultGrid = Seq.empty[Seq[GreyScalePixel]]
    resultGrid = resultGrid :+ resultColumns
    resultGrid = resultGrid :+ resultColumns

    val destGrid = new PixelGrid[GreyScalePixel](resultGrid)

    assert(result.compare(destGrid))

  }
}
