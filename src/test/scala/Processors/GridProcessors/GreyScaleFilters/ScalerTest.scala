package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import org.scalatest.flatspec.AnyFlatSpec

class ScalerTest  extends AnyFlatSpec {
  "FontAspectRatio(scale: 2) " should "should change the size to 10x10" in {
    val columns = Seq(GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(3), GreyScalePixel(4), GreyScalePixel(5))
    val resultColumns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2), GreyScalePixel(3),
      GreyScalePixel(3), GreyScalePixel(4), GreyScalePixel(4), GreyScalePixel(5), GreyScalePixel(5))
    var grid = Seq.empty[Seq[GreyScalePixel]]
    for (i <- 0 until 5) {
      val tmp = if (i % 2 == 0) columns else columns.reverse
      grid = grid :+ tmp
    }

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val scaledGrid = new Scaler(2).apply(originalGrid)

    var resultGrid = Seq.empty[Seq[GreyScalePixel]]
    for (i <- 0 until 5) {
      val tmp = if (i % 2 == 0) resultColumns else resultColumns.reverse
      resultGrid = resultGrid :+ tmp
      resultGrid = resultGrid :+ tmp
    }

    val destGrid = new PixelGrid[GreyScalePixel](resultGrid)

    assert(scaledGrid.compare(destGrid))
  }

  "FontAspectRatio(scale: 3.6) " should "should change the size to 20x20" in {
    val columns = Seq(GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(3), GreyScalePixel(4), GreyScalePixel(5))
    val resultColumns = Seq(
      GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(1),
      GreyScalePixel(2), GreyScalePixel(2), GreyScalePixel(2), GreyScalePixel(2),
      GreyScalePixel(3), GreyScalePixel(3), GreyScalePixel(3), GreyScalePixel(3),
      GreyScalePixel(4), GreyScalePixel(4), GreyScalePixel(4), GreyScalePixel(4),
      GreyScalePixel(5), GreyScalePixel(5), GreyScalePixel(5), GreyScalePixel(5))
    var grid = Seq.empty[Seq[GreyScalePixel]]
    for (i <- 0 until 5) {
      val tmp = if (i % 2 == 0) columns else columns.reverse
      grid = grid :+ tmp
    }

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val scaledGrid = new Scaler(3.6).apply(originalGrid)

    var resultGrid = Seq.empty[Seq[GreyScalePixel]]
    for (i <- 0 until 5) {
      val tmp = if (i % 2 == 0) resultColumns else resultColumns.reverse
      resultGrid = resultGrid :+ tmp
      resultGrid = resultGrid :+ tmp
      resultGrid = resultGrid :+ tmp
      resultGrid = resultGrid :+ tmp
    }

    val destGrid = new PixelGrid[GreyScalePixel](resultGrid)

    assert(scaledGrid.compare(destGrid))

  }

  "FontAspectRatio(scale: 0.5) " should "should change the size to 2x2" in {
    val columns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2))
    val columns2 = Seq(GreyScalePixel(3), GreyScalePixel(3), GreyScalePixel(4), GreyScalePixel(4))

    val resultColumns = Seq(GreyScalePixel(1), GreyScalePixel(2))
    val resultColumns2 = Seq(GreyScalePixel(3), GreyScalePixel(4))

    var grid = Seq.empty[Seq[GreyScalePixel]]
    grid = grid :+ columns
    grid = grid :+ columns
    grid = grid :+ columns2
    grid = grid :+ columns2

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val scaledGrid = new Scaler(0.5).apply(originalGrid)

    var resultGrid = Seq.empty[Seq[GreyScalePixel]]

    resultGrid = resultGrid :+ resultColumns
    resultGrid = resultGrid :+ resultColumns2


    val destGrid = new PixelGrid[GreyScalePixel](resultGrid)

    assert(scaledGrid.compare(destGrid))

  }

  "FontAspectRatio(scale: 0.25) " should "should change the size to 1x1" in {
    val columns = Seq(GreyScalePixel(1), GreyScalePixel(1), GreyScalePixel(2), GreyScalePixel(2))
    val columns2 = Seq(GreyScalePixel(3), GreyScalePixel(3), GreyScalePixel(4), GreyScalePixel(4))

    val resultColumns = Seq(GreyScalePixel(2))

    var grid = Seq.empty[Seq[GreyScalePixel]]
    grid = grid :+ columns
    grid = grid :+ columns
    grid = grid :+ columns2
    grid = grid :+ columns2

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val scaledGrid = new Scaler(0.25).apply(originalGrid)

    var resultGrid = Seq.empty[Seq[GreyScalePixel]]

    resultGrid = resultGrid :+ resultColumns


    val destGrid = new PixelGrid[GreyScalePixel](resultGrid)

    assert(scaledGrid.compare(destGrid))

  }
}