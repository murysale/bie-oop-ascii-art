package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import org.scalatest.flatspec.AnyFlatSpec
import Processors.GridProcessors.GreyScaleFilters.Invert

class InvertTest extends AnyFlatSpec {
  "Invert " should "invert all pixels (0, 255, 122)" in {
    val columnsSource = Seq(GreyScalePixel(0), GreyScalePixel(255), GreyScalePixel(122))
    var gridSource = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      gridSource = gridSource :+ columnsSource
    }

    val originalGrid = new PixelGrid[GreyScalePixel](gridSource)
    val result = new Invert().apply(originalGrid)

    val columnsDest = Seq(GreyScalePixel(255), GreyScalePixel(0), GreyScalePixel(133))
    var destSeq = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      destSeq = destSeq :+ columnsDest
    }

    val destGrid = new PixelGrid[GreyScalePixel](destSeq)

    assert(result.compare(destGrid))
  }

  "Invert " should "invert all pixels(233, 11, 4)" in {
    val columnsSource = Seq(GreyScalePixel(233), GreyScalePixel(11), GreyScalePixel(4))
    var gridSource = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      gridSource = gridSource :+ columnsSource
    }

    val originalGrid = new PixelGrid[GreyScalePixel](gridSource)
    val result = new Invert().apply(originalGrid)

    val columnsDest = Seq(GreyScalePixel(22), GreyScalePixel(244), GreyScalePixel(251))
    var destSeq = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      destSeq = destSeq :+ columnsDest
    }

    val destGrid = new PixelGrid[GreyScalePixel](destSeq)

    assert(result.compare(destGrid))
  }
}
