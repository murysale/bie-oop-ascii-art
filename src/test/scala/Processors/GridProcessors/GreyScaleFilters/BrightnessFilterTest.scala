package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import org.scalatest.flatspec.AnyFlatSpec

class BrightnessFilterTest extends AnyFlatSpec {
  "BrightnessFilter(0) " should "not change input" in {
    val columns = Seq(GreyScalePixel(0), GreyScalePixel(0), GreyScalePixel(0))
    var grid = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      grid = grid :+ columns
    }

    val originalGrid = new PixelGrid[GreyScalePixel](grid)
    val result = new BrightnessFilter(0).apply(originalGrid)

    assert(result.compare(originalGrid))
  }

  "BrightnessFilter(10000) " should "set all pixels to 255" in {
    val columnsSource = Seq(GreyScalePixel(0), GreyScalePixel(0), GreyScalePixel(0))
    var gridSource = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      gridSource = gridSource :+ columnsSource
    }

    val originalGrid = new PixelGrid[GreyScalePixel](gridSource)
    val result = new BrightnessFilter(10000).apply(originalGrid)

    val columnsDest = Seq(GreyScalePixel(255), GreyScalePixel(255), GreyScalePixel(255))
    var destSeq = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      destSeq = destSeq :+ columnsDest
    }

    val destGrid = new PixelGrid[GreyScalePixel](destSeq)

    assert(result.compare(destGrid))
  }

  "BrightnessFilter(-9999) " should "set all pixels to 0" in {
    val columnsSource = Seq(GreyScalePixel(255), GreyScalePixel(255), GreyScalePixel(255))
    var gridSource = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      gridSource = gridSource :+ columnsSource
    }

    val originalGrid = new PixelGrid[GreyScalePixel](gridSource)
    val result = new BrightnessFilter(-9999).apply(originalGrid)

    val columnsDest = Seq(GreyScalePixel(0), GreyScalePixel(0), GreyScalePixel(0))
    var destSeq = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      destSeq = destSeq :+ columnsDest
    }

    val destGrid = new PixelGrid[GreyScalePixel](destSeq)

    assert(result.compare(destGrid))
  }

  "BrightnessFilter(10) " should "increase the value of all pixels by 10" in {
    val columnsSource = Seq(GreyScalePixel(23), GreyScalePixel(44), GreyScalePixel(55))
    var gridSource = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      gridSource = gridSource :+ columnsSource
    }

    val originalGrid = new PixelGrid[GreyScalePixel](gridSource)
    val result = new BrightnessFilter(10).apply(originalGrid)

    val columnsDest = Seq(GreyScalePixel(33), GreyScalePixel(54), GreyScalePixel(65))
    var destSeq = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      destSeq = destSeq :+ columnsDest
    }

    val destGrid = new PixelGrid[GreyScalePixel](destSeq)

    assert(result.compare(destGrid))
  }

  "BrightnessFilter(-10) " should "decrease the value of all pixels by 10" in {
    val columnsSource = Seq(GreyScalePixel(23), GreyScalePixel(44), GreyScalePixel(55))
    var gridSource = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      gridSource = gridSource :+ columnsSource
    }

    val originalGrid = new PixelGrid[GreyScalePixel](gridSource)
    val result = new BrightnessFilter(-10).apply(originalGrid)

    val columnsDest = Seq(GreyScalePixel(13), GreyScalePixel(34), GreyScalePixel(45))
    var destSeq = Seq.empty[Seq[GreyScalePixel]]

    for (_ <- 0 until 3) {
      destSeq = destSeq :+ columnsDest
    }

    val destGrid = new PixelGrid[GreyScalePixel](destSeq)

    assert(result.compare(destGrid))
  }
}
