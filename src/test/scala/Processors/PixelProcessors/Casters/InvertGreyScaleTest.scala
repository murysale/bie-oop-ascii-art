package Processors.PixelProcessors.Casters

import Models.Pixel.GreyScalePixel
import org.scalatest.flatspec.AnyFlatSpec

class InvertGreyScaleTest extends AnyFlatSpec {
  "InvertGreyScaleTest" should "invert GreyScalePixels" in {
    val inverter = new InvertGreyScale()
    val px1 = GreyScalePixel(1)
    val px2 = GreyScalePixel(50)
    val px3 = GreyScalePixel(46)
    val px4 = GreyScalePixel(255)
    val px5 = GreyScalePixel(0)

    assert(inverter.cast(px1).scale == 254)
    assert(inverter.cast(px2).scale == 205)
    assert(inverter.cast(px3).scale == 209)
    assert(inverter.cast(px4).scale == 0)
    assert(inverter.cast(px5).scale == 255)
  }
}
