package Processors.PixelProcessors.Casters

import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class RGBAToGreyScaleTest extends AnyFlatSpec  {
  "RGBAToGreyScale" should "convert rgba pixel to grey scale pixel" in {
    val caster = new RGBAToGreyScale
    val rgbaPx1 = RGBAPixel(0)
    val rgbaPx2 = RGBAPixel(1283089)
    val rgbaPx3 = RGBAPixel(-234)
    val rgbaPx4 = RGBAPixel(-12212)

    assert(caster.cast(rgbaPx1).scale === 0)
    assert(caster.cast(rgbaPx2).scale === 95)
    assert(caster.cast(rgbaPx3).scale === 229)
    assert(caster.cast(rgbaPx4).scale === 208)

  }
}
