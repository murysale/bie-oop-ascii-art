package Processors.PixelProcessors.Casters

import Models.Pixel.GreyScalePixel
import org.scalatest.flatspec.AnyFlatSpec

class GreyScaleBrightnessTest extends AnyFlatSpec {
  "GreyScaleBrightness(1)" should "increase Brightness by 1" in {
    val brightnessCaster = new GreyScaleBrightness(1)
    val px1 = GreyScalePixel(1)
    val px2 = GreyScalePixel(6)
    val px3 = GreyScalePixel(46)
    val px4 = GreyScalePixel(255)
    val px5 = GreyScalePixel(0)

    assert(brightnessCaster.cast(px1).scale == 2)
    assert(brightnessCaster.cast(px2).scale == 7)
    assert(brightnessCaster.cast(px3).scale == 47)
    assert(brightnessCaster.cast(px4).scale == 255)
    assert(brightnessCaster.cast(px5).scale == 1)
  }

  "GreyScaleBrightness(3434)" should "max the brightness out" in {
    val brightnessCaster = new GreyScaleBrightness(3434)
    val px1 = GreyScalePixel(1)
    val px2 = GreyScalePixel(6)
    val px3 = GreyScalePixel(46)
    val px4 = GreyScalePixel(255)
    val px5 = GreyScalePixel(0)

    assert(brightnessCaster.cast(px1).scale == 255)
    assert(brightnessCaster.cast(px2).scale == 255)
    assert(brightnessCaster.cast(px3).scale == 255)
    assert(brightnessCaster.cast(px4).scale == 255)
    assert(brightnessCaster.cast(px5).scale == 255)
  }

  "GreyScaleBrightness(-5)" should "decrease the brightness by 5" in {
    val brightnessCaster = new GreyScaleBrightness(-5)
    val px1 = GreyScalePixel(1)
    val px2 = GreyScalePixel(6)
    val px3 = GreyScalePixel(46)
    val px4 = GreyScalePixel(255)
    val px5 = GreyScalePixel(0)

    assert(brightnessCaster.cast(px1).scale == 0)
    assert(brightnessCaster.cast(px2).scale == 1)
    assert(brightnessCaster.cast(px3).scale == 41)
    assert(brightnessCaster.cast(px4).scale == 250)
    assert(brightnessCaster.cast(px5).scale == 0)
  }

  "GreyScaleBrightness(-5000)" should "null the brightness" in {
    val brightnessCaster = new GreyScaleBrightness(-5000)
    val px1 = GreyScalePixel(1)
    val px2 = GreyScalePixel(6)
    val px3 = GreyScalePixel(46)
    val px4 = GreyScalePixel(255)
    val px5 = GreyScalePixel(0)

    assert(brightnessCaster.cast(px1).scale == 0)
    assert(brightnessCaster.cast(px2).scale == 0)
    assert(brightnessCaster.cast(px3).scale == 0)
    assert(brightnessCaster.cast(px4).scale == 0)
    assert(brightnessCaster.cast(px5).scale == 0)
  }
}
