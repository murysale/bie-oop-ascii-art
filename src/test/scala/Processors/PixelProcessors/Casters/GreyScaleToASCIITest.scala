package Processors.PixelProcessors.Casters

import Models.Pixel.GreyScalePixel
import org.scalatest.flatspec.AnyFlatSpec

class GreyScaleToASCIITest extends AnyFlatSpec  {
  "GreyScaleToASCII" should "convert grey scale pixel to ascii pixel" in {
    val table = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. "
    val caster = new GreyScaleToASCII(table)
    val gsPx1 = GreyScalePixel(0)
    val gsPx2 = GreyScalePixel(1)
    val gsPx3 = GreyScalePixel(2)
    val gsPx4 = GreyScalePixel(8)
    val gsPx5 = GreyScalePixel(100)
    val gsPx6 = GreyScalePixel(255)


    assert(caster.cast(gsPx1).value === '$')
    assert(caster.cast(gsPx2).value === '$')
    assert(caster.cast(gsPx3).value === '$')
    assert(caster.cast(gsPx4).value === 'B')
    assert(caster.cast(gsPx5).value === 'U')
    assert(caster.cast(gsPx6).value === ' ')

  }
}
