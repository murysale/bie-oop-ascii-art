
import org.scalatest.flatspec.AnyFlatSpec
import UI.ConsoleUI
import org.scalatest.PrivateMethodTester

class UITester extends AnyFlatSpec with PrivateMethodTester {
  "console UI" should "parse arguments" in {
    val consoleUI = new ConsoleUI

    val parseArguments = PrivateMethod[Seq[(String, String)]](Symbol("parseArguments"))
    val args = Seq("--image-random", "--scale", "0.7", "--font-aspect-ratio", "1:2", "--table", "sharp", "--flip", "y")

    //val ex1 = consoleUI.start(Seq("--image-random", "--scale", "0.7", "--font-aspect-ratio", "1:2", "--table", "sharp", "--flip", "y"))
    val res = consoleUI invokePrivate parseArguments(args)
    assert(Seq(("--image-random",""),("--scale", "0.7"), ("--font-aspect-ratio", "1:2"), ("--table", "sharp"), ("--flip", "y")) === res)

    val args2 = Seq("--image C:\\Users\\qdatu\\IdeaProjects\\ASCII_Art\\src\\test\\Images\\planet.png", "--scale", "0.08", "--font-aspect-ratio", "1:2", "--brightness", "+10", "--table", "extended")
    val res2 = consoleUI invokePrivate parseArguments(args2)
    assert(Seq(("--image C:\\Users\\qdatu\\IdeaProjects\\ASCII_Art\\src\\test\\Images\\planet.png",""),("--scale", "0.08"), ("--font-aspect-ratio", "1:2"), ("--brightness", "+10"), ("--table", "extended")) === res2)

  }


}
