package Exporters

import MediaLoaders.ImageLoaders.{LocalImageLoader, RandomImageLoader}
import Models.Pixel.{ASCIIPixel, GreyScalePixel, RGBAPixel}
import Models.PixelGrid
import Processors.GridProcessors.GridCaster
import Processors.PixelProcessors.Casters.{GreyScaleToASCII, RGBAToGreyScale}
import org.scalatest.flatspec.AnyFlatSpec

class FileExporterTest extends AnyFlatSpec {
  def genString(value: PixelGrid[ASCIIPixel]): String = {
    var result = ""
    for (x <- value.image) {
      for (y <- x) result += y.value
      result += '\n'
    }
    result
  }

  "Random Test1" should "should export text to file" in {
    val exporter = new FileExporter("src//test//outputs//out.txt")
    val image = new RandomImageLoader().load()

    val gridProcessor1 = new GridCaster[RGBAPixel, GreyScalePixel](new RGBAToGreyScale())

    val greyScaleImage = gridProcessor1.cast(image)
    val gridProcessor2 = new GridCaster[GreyScalePixel, ASCIIPixel](new GreyScaleToASCII(" .:-=+*#%@"))
    val ASCIIImage = gridProcessor2.cast(greyScaleImage)

    exporter.`export`(ASCIIImage)

    var ASCIIstring = genString(ASCIIImage)
    ASCIIstring = ASCIIstring.substring(0, ASCIIstring.length - 1)
    val source = scala.io.Source.fromFile("src//test//outputs//out.txt")
    val resString = source.getLines.mkString("\n")

    assert(ASCIIstring == resString)
  }
}
