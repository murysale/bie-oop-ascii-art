package Exporters

import MediaLoaders.ImageLoaders.RandomImageLoader
import Models.Pixel.{ASCIIPixel, GreyScalePixel, RGBAPixel}
import Models.PixelGrid
import Processors.GridProcessors.GridCaster
import Processors.PixelProcessors.Casters.{GreyScaleToASCII, RGBAToGreyScale}
import org.scalatest.flatspec.AnyFlatSpec

class CLIExporterTest extends AnyFlatSpec {
  def genString(value: PixelGrid[ASCIIPixel]): String = {
    var result = ""
    for (x <- value.image) {
      for (y <- x) result += y.value
      result += '\n'
    }
    result
  }

  "Random Test1" should "print ascii pixels" in {
    val stream = new java.io.ByteArrayOutputStream()
    val exporter = new CLIExporter()
    val image = new RandomImageLoader().load()

    val gridProcessor1 = new GridCaster[RGBAPixel, GreyScalePixel](new RGBAToGreyScale())

    val greyScaleImage = gridProcessor1.cast(image)
    val gridProcessor2 = new GridCaster[GreyScalePixel, ASCIIPixel](new GreyScaleToASCII(" .:-=+*#%@"))
    val ASCIIImage = gridProcessor2.cast(greyScaleImage)

    Console.withOut(stream) {
      exporter.`export`(ASCIIImage)
    }

    var streamStr = stream.toString
    val genStr = genString(ASCIIImage)

    streamStr = streamStr.substring(0, streamStr.length - 2)
    assert(streamStr == genStr)

  }

  "Random Test2" should "print ascii pixels" in {
    val stream = new java.io.ByteArrayOutputStream()
    val exporter = new CLIExporter()
    val image = new RandomImageLoader().load()

    val gridProcessor1 = new GridCaster[RGBAPixel, GreyScalePixel](new RGBAToGreyScale())

    val greyScaleImage = gridProcessor1.cast(image)
    val gridProcessor2 = new GridCaster[GreyScalePixel, ASCIIPixel](new GreyScaleToASCII(" .:-=+*#%@"))
    val ASCIIImage = gridProcessor2.cast(greyScaleImage)

    Console.withOut(stream) {
      exporter.`export`(ASCIIImage)
    }

    var streamStr = stream.toString
    val genStr = genString(ASCIIImage)

    streamStr = streamStr.substring(0, streamStr.length - 2)
    assert(streamStr == genStr)
  }

  "Random Test3" should "print ascii pixels" in {
    val stream = new java.io.ByteArrayOutputStream()
    val exporter = new CLIExporter()
    val image = new RandomImageLoader().load()

    val gridProcessor1 = new GridCaster[RGBAPixel, GreyScalePixel](new RGBAToGreyScale())

    val greyScaleImage = gridProcessor1.cast(image)
    val gridProcessor2 = new GridCaster[GreyScalePixel, ASCIIPixel](new GreyScaleToASCII(" .:-=+*#%@"))
    val ASCIIImage = gridProcessor2.cast(greyScaleImage)

    Console.withOut(stream) {
      exporter.`export`(ASCIIImage)
    }

    var streamStr = stream.toString
    val genStr = genString(ASCIIImage)

    streamStr = streamStr.substring(0, streamStr.length - 2)
    assert(streamStr == genStr)
  }

  "Random Test4" should "print ascii pixels" in {
    val stream = new java.io.ByteArrayOutputStream()
    val exporter = new CLIExporter()
    val image = new RandomImageLoader().load()

    val gridProcessor1 = new GridCaster[RGBAPixel, GreyScalePixel](new RGBAToGreyScale())

    val greyScaleImage = gridProcessor1.cast(image)
    val gridProcessor2 = new GridCaster[GreyScalePixel, ASCIIPixel](new GreyScaleToASCII(" .:-=+*#%@"))
    val ASCIIImage = gridProcessor2.cast(greyScaleImage)

    Console.withOut(stream) {
      exporter.`export`(ASCIIImage)
    }

    var streamStr = stream.toString
    val genStr = genString(ASCIIImage)

    streamStr = streamStr.substring(0, streamStr.length - 2)
    assert(streamStr == genStr)

  }
}
