package ImageLoaders.LocalImageLoaders

import MediaLoaders.ImageLoaders.LocalImageLoaders.LocalPNGImageLoader
import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class LocalPNGImageLoaderTest extends AnyFlatSpec {
  "LocalJPGImageLoader(path=...png)" should "import a png file" in {
    val loader = new LocalPNGImageLoader("src//test//Images//test20x20.png")
    val result = loader.load()
    assert(result.getHeight == 20)
    assert(result.getWidth == 20)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  it should "throw an exception if format is jpg" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalPNGImageLoader("src//test//Images//trollface.jpg")
      loader.load()
    }
  }

  it should "throw an exception if format is webp" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalPNGImageLoader("src//test//Images//planet.webp")
      loader.load()
    }
  }

  it should "throw an exception if format is gif" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalPNGImageLoader("src//test//Images//smile.gif")
      loader.load()
    }
  }
}
