package ImageLoaders.LocalImageLoaders

import MediaLoaders.ImageLoaders.LocalImageLoaders.LocalGIFImageLoader
import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class LocalGIFImageLoaderTest extends AnyFlatSpec {
  "LocalGIFImageLoader(path=...gif)" should "import a gif file" in {
    val loader = new LocalGIFImageLoader("src//test//Images//smile.gif")
    val result = loader.load()
    assert(result.getHeight == 20)
    assert(result.getWidth == 20)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  it should "throw an exception if format is png" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalGIFImageLoader("src//test//Images//planet.png")
      loader.load()
    }
  }

  it should "throw an exception if format is webp" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalGIFImageLoader("src//test//Images//planet.webp")
      loader.load()
    }
  }

  it should "throw an exception if format is jpg" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalGIFImageLoader("src//test//Images//anya.jpg")
      loader.load()
    }
  }
}
