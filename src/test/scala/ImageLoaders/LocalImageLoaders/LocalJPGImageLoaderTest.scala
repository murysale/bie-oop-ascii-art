package ImageLoaders.LocalImageLoaders

import MediaLoaders.ImageLoaders.LocalImageLoaders.LocalJPGImageLoader
import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class LocalJPGImageLoaderTest extends AnyFlatSpec {
  "LocalJPGImageLoader(path=...jpg)" should "import a jpg file" in {
    val loader = new LocalJPGImageLoader("src//test//Images//test20x10.jpg")
    val result = loader.load()
    assert(result.getHeight == 20)
    assert(result.getWidth == 10)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  it should "throw an exception if format is png" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalJPGImageLoader("src//test//Images//planet.png")
      loader.load()
    }
  }

  it should "throw an exception if format is webp" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalJPGImageLoader("src//test//Images//planet.webp")
      loader.load()
    }
  }

  it should "throw an exception if format is gif" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalJPGImageLoader("src//test//Images//smile.gif")
      loader.load()
    }
  }
}
