package ImageLoaders.LocalImageLoaders

import MediaLoaders.ImageLoaders.LocalImageLoaders.ImageIOImageLoader
import org.scalatest.flatspec.AnyFlatSpec

class ImageIOImageLoaderTest extends AnyFlatSpec {
  "ImageIOImageLoader(path=...png)" should "load image as a pixel grid" in {
    val img = new ImageIOImageLoader("src//test//Images//test20x20.png").load()
    assert(img.getWidth == 20)
    assert(img.getHeight == 20)
  }

  "ImageIOImageLoader(path=...bmp)" should "load image as a pixel grid" in {
    val img = new ImageIOImageLoader("src//test//Images//life.bmp").load()
    assert(img.getWidth == 200)
    assert(img.getHeight == 190)
  }

  "ImageIOImageLoader(path=...jpg)" should "load image as a pixel grid" in {
    val img = new ImageIOImageLoader("src//test//Images//test20x10.jpg").load()
    assert(img.getWidth == 10)
    assert(img.getHeight == 20)
  }

  "ImageIOImageLoader(path=...gif)" should "load image as a pixel grid" in {
    val img = new ImageIOImageLoader("src//test//Images//smile.gif").load()
    assert(img.getWidth == 20)
    assert(img.getHeight == 20)
  }
}
