package ImageLoaders.LocalImageLoaders

import MediaLoaders.ImageLoaders.LocalImageLoaders.LocalBMPImageLoader
import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class LocalBMPImageLoaderTest extends AnyFlatSpec  {
  "LocalBMPImageLoader(path=...bmp)" should "import a bmp file" in {
    val loader = new LocalBMPImageLoader("src//test//Images//life.bmp")
    val result = loader.load()
    assert(result.getHeight == 190)
    assert(result.getWidth == 200)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  it should "throw an exception if format is png" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalBMPImageLoader("src//test//Images//planet.png")
      loader.load()
    }
  }

  it should "throw an exception if format is webp" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalBMPImageLoader("src//test//Images//planet.webp")
      loader.load()
    }
  }

  it should "throw an exception if format is jpg" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalBMPImageLoader("src//test//Images//anya.jpg")
      loader.load()
    }
  }
}
