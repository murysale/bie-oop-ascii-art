package ImageLoaders

import MediaLoaders.ImageLoaders.LocalImageLoader
import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class LocalImageLoaderTest extends AnyFlatSpec {

  "LocalImageLoader(path=...test20x10.jpg)" should "import a 20x10 jpg file" in {
    val loader = new LocalImageLoader("src//test//Images//test20x10.jpg")
    val result = loader.load()
    assert(result.getHeight == 20)
    assert(result.getWidth == 10)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  "LocalImageLoader(path=...test20x20.jpg)" should "import a png file" in {
    val loader = new LocalImageLoader("src//test//Images//test20x20.png")
    val result = loader.load()
    assert(result.getHeight == 20)
    assert(result.getWidth == 20)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  "LocalImageLoader(path=...gif)" should "import a gif file" in {
    val loader = new LocalImageLoader("src//test//Images//smile.gif")
    val result = loader.load()
    assert(result.getHeight == 20)
    assert(result.getWidth == 20)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  "LocalImageLoader(path=...bmp)" should "import a bmp file" in {
    val loader = new LocalImageLoader("src//test//Images//life.bmp")
    val result = loader.load()
    assert(result.getHeight == 190)
    assert(result.getWidth == 200)

    for (y <- result.image)
      for (x <- y) {
        assert(x.isInstanceOf[RGBAPixel])
      }
  }

  it should "throw an exception if format's unsupported" in {
    assertThrows[IllegalArgumentException] {
      val loader = new LocalImageLoader("src//test//Images//planet.webp")
      loader.load()
    }
  }

}
