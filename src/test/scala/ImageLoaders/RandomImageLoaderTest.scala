import MediaLoaders.ImageLoaders.RandomImageLoader
import Models.Pixel.RGBAPixel
import org.scalatest.flatspec.AnyFlatSpec

class RandomImageLoaderTest extends AnyFlatSpec  {
  "RandomImageLoader" should "return a random PixelGrid[RGBAPixel] of certain size" in {
    val loader = new RandomImageLoader
    val result = loader.load()
    assert(result.getHeight >= 10 && result.getHeight <= 100)
    assert(result.getWidth >= 10 && result.getWidth <= 100)

    for (y <- 0 until result.getHeight)
      for(x <- 0 until result.getWidth) {
        assert(result.image(y)(x).isInstanceOf[RGBAPixel])
      }
  }

}
