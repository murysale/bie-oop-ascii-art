package Executors

trait Executor {
  def execute: Unit
}
