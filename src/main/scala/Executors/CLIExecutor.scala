package Executors

import Exporters.Exporter
import MediaLoaders.ImageLoaders.ImageLoader
import Processors.GridProcessors.{Filter, GridCaster}
import Processors.PixelProcessors.Casters.{GreyScaleToASCII, RGBAToGreyScale}
import Models.Pixel.{ASCIIPixel, GreyScalePixel, RGBAPixel}
import Models.PixelGrid

/*

    Executors Package overall is an operational
    unit which accepts parsed commands, and
    invokes functionality to perform them.

    This class implements the execution logic of the app

 */

class CLIExecutor(sourceImage: PixelGrid[RGBAPixel],
                  charTable: String,
                  filters: Seq[Filter[GreyScalePixel]],
                  exporters: Seq[Exporter]) extends Executor
{
  override def execute: Unit = {
    val gridProcessor1 = new GridCaster[RGBAPixel, GreyScalePixel](new RGBAToGreyScale())

    val greyScaleImage = gridProcessor1.cast(sourceImage)
    var filteredImage = greyScaleImage
    for (f <- filters) filteredImage = f.apply(filteredImage)
    val gridProcessor2 = new GridCaster[GreyScalePixel, ASCIIPixel](new GreyScaleToASCII(charTable))
    val ASCIIImage = gridProcessor2.cast(filteredImage)

    for (ex <- exporters) ex.export(ASCIIImage)

  }

}
