package Models

/*

    Data Model for storing and transferring an image
    across the app. Internal 2D array is immutable.
    In a case of changing the internal pixel representation
    to a more sophisticated data structure

    val image might be renamed

    and the method image could be created for accessing
    the internal storage and returning it as Seq[Seq[T]]

 */

class PixelGrid[T](val image: Seq[Seq[T]]) {
  def getHeight: Int = image.length
  def getWidth: Int = if (this.getHeight > 0) image.head.length else 0
  def compare(pixelGrid: PixelGrid[T]): Boolean = {
    if(this.getWidth != pixelGrid.getWidth) return false
    if(this.getHeight != pixelGrid.getHeight) return false

    for(row <- 0 until pixelGrid.getHeight) {
      for(column <- 0 until pixelGrid.getWidth) {
        if(this.image(row)(column) != pixelGrid.image(row)(column))
          return false
      }
    }

    true
  }
}
