package Models.Pixel

// Representation of grey scale

case class GreyScalePixel(scale: Int) extends Pixel{
  if (scale < 0 || scale > 255)
    throw new IllegalArgumentException("Error: Scale has to be between 0 and 255!")
  override def getValue: Int = scale
}
