package Models.Pixel

//representation of an ASCII Pixel

case class ASCIIPixel(value: Char) extends Pixel {
  if (value < 0 || value > 255)
    throw new IllegalArgumentException("Error: ASCII code has to be between 0 and 127!")
  override def getValue: Int = value.toInt
}
