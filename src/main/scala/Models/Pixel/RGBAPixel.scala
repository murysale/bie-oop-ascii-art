package Models.Pixel

/*
    This class stores the RGBA value
    of the pixel and provides methods
    to access each color.
    Possible to easily modify behaviour
    to work with alpha.
 */

case class RGBAPixel(color: Int) extends Pixel {
  //No need to check if Int is valid
  def alpha(): Int = (color >> 24) & 0xff
  def red(): Int = (color >> 16) & 0xff
  def green(): Int = (color >> 8) & 0xff
  def blue(): Int = color & 0xff

  override def getValue: Int = color
}
