package Processors.PixelProcessors.Casters

import Processors.PixelProcessors.PixelCaster
import Models.Pixel.GreyScalePixel

/*
    Class inverting GreyScalePixels
 */

class GreyScaleBrightness(value: Int) extends PixelCaster[GreyScalePixel, GreyScalePixel] {
  override def cast (px: GreyScalePixel): GreyScalePixel = {
    var tmp = px.scale + value
    if (tmp < 0) tmp = 0
    if (tmp > 255) tmp = 255
    GreyScalePixel(tmp)
  }
}
