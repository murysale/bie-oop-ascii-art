package Processors.PixelProcessors.Casters

import Processors.PixelProcessors.PixelCaster
import Models.Pixel.GreyScalePixel

/*
    Class inverting GreyScalePixels
 */

class InvertGreyScale() extends PixelCaster[GreyScalePixel, GreyScalePixel] {
  override def cast (px: GreyScalePixel): GreyScalePixel = GreyScalePixel(255 - px.scale)
}
