package Processors.PixelProcessors.Casters

import Processors.PixelProcessors.PixelCaster
import Models.Pixel.{GreyScalePixel, RGBAPixel}

/*
    Class transforming RGBA pixels to GreyScale pixels
 */

class RGBAToGreyScale() extends PixelCaster[RGBAPixel, GreyScalePixel] {
  override def cast (px: RGBAPixel): GreyScalePixel = {
    val gs = java.lang.Math.round(px.red() * 0.3 + px.green() * 0.59 + px.blue() * 0.11)
    if (gs < 0 || gs > 255) throw new IllegalArgumentException("Illegal Pixel's occurred!")
    GreyScalePixel(gs.toInt)
  }

}
