package Processors.PixelProcessors.Casters

import Processors.PixelProcessors.PixelCaster
import Models.Pixel.{ASCIIPixel, GreyScalePixel}

/*

    Class, transforming GreyScale pixels
    to ASCII pixels

 */

class GreyScaleToASCII(charMap: Seq[Char]) extends PixelCaster[GreyScalePixel, ASCIIPixel]{
  override def cast(px: GreyScalePixel): ASCIIPixel = {

    ASCIIPixel(charMap((charMap.length - 1) * px.scale / 255))

  }
}
