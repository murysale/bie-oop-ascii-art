package Processors.PixelProcessors

import Models.Pixel.Pixel

/*
    General Interface for the Pixel
    casting
 */

trait PixelCaster[T <: Pixel, Y <: Pixel] {
  def cast(px: T): Y
}
