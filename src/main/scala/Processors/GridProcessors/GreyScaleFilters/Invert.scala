package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import Processors.GridProcessors.{Filter, GridCaster}
import Processors.PixelProcessors.Casters.{GreyScaleBrightness, InvertGreyScale}

//Same as Brightness filter, but for inversion. Suffers the same issue

class Invert extends Filter[GreyScalePixel] {
  override def apply(grid: PixelGrid[GreyScalePixel]): PixelGrid[GreyScalePixel] = {
    val invertPixelCaster = new InvertGreyScale
    val invertGridCaster = new GridCaster[GreyScalePixel, GreyScalePixel](invertPixelCaster)
    invertGridCaster.cast(grid)
  }
}
