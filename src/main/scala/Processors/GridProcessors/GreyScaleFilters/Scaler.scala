package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import Processors.GridProcessors.Filter
/*

    Scaler scales PixelGrids!

    Constructor accepts filter parameters.
    This way class may be reused for the potential
    multiple-image input

 */

class Scaler(scale: Double) extends Filter[GreyScalePixel]{
  override def apply(grid: PixelGrid[GreyScalePixel]): PixelGrid[GreyScalePixel] = {
    if (scale < 0) throw new IllegalArgumentException("Scale can't be less then zero")
    if (scale == 1) return grid
    if (scale < 1) return reduceGrid(grid)
    upscaleGrid(grid)
  }

  //if scale < 1
  private def reduceGrid(grid: PixelGrid[GreyScalePixel]): PixelGrid[GreyScalePixel] = {
    val gridHeight: Int = grid.image.length
    val gridWidth: Int = grid.image.head.length
    val newHeight: Int = java.lang.Math.round(gridHeight * scale).toInt
    val newWidth: Int = java.lang.Math.round(gridWidth * scale).toInt

    if (newWidth == gridWidth && gridHeight == newHeight) return grid

    val h_chunk = Math.round(gridHeight.toFloat / newHeight)
    val w_chunk = Math.round(gridWidth.toFloat / newWidth)

    var buffer_w = List.empty[Int]
    var buffer_h = List.empty[List[Int]]
    var count_w, count_h = 0
    var result = Seq.empty[Seq[GreyScalePixel]]
    var real_height = 0

    /*

        Here I used a "chunk approach", where I determined
        the size of subgrids of pixels and calculated them

     */
       
    for (y <- grid.image) {
      var sum_x =  0
      for (x <- y) {
        sum_x += x.getValue
        count_w += 1
        if (count_w == w_chunk) {
          count_w = 0
          buffer_w = buffer_w :+ sum_x / w_chunk
          sum_x = 0
        }
      }
      if (sum_x != 0) {
        count_w = 0
        sum_x = 0
      }
      buffer_h = buffer_h :+ buffer_w

      buffer_w = List.empty[Int]
      count_h += 1
      if (count_h == h_chunk) {
        count_h = 0
        var res_row = Seq.empty[GreyScalePixel]
        for(i <- buffer_h.head.indices) {
          var sum_h = 0
          for(h <- buffer_h) sum_h += h(i)
          //println(f"${sum_h} / ${h_chunk} = ${sum_h / h_chunk}")
          res_row = res_row :+ GreyScalePixel(sum_h / h_chunk)
          //println(res_row)

        }
        result = result :+ res_row
        real_height += 1
        buffer_h = List.empty[List[Int]]
      }


    }
    new PixelGrid[GreyScalePixel](result)
  }

  //if scale > 1
  private def upscaleGrid(grid: PixelGrid[GreyScalePixel]): PixelGrid[GreyScalePixel] = {
    val multiplier = java.lang.Math.round(scale).toInt
//    println(f"New resolution: ${newWidth}x${newHeight}")

    /*

        In this function I repeated values of pixels according to scale
        Accepts only integers. If not an integer's passed, rounds it

     */
    var result = Seq.empty[Seq[GreyScalePixel]]

    for(y <- grid.image) {
      var buffer_w = Seq.empty[GreyScalePixel]
      for(x <- y) {
        for(_ <- 0 until multiplier) buffer_w = buffer_w :+ x
      }
      for(_ <- 0 until multiplier) result = result :+ buffer_w
    }

    new PixelGrid[GreyScalePixel](result)
  }
}
