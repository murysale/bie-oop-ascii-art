package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import Processors.GridProcessors.Filter

/*

    This class changes PixelGrid's aspect
    ratio. The algorithm is as simple as inefficient
    I use GreyScales here since we need to change
    pixels' values and it is the layer,
    which is unlikely to be removed

 */

class FontAspectRatio(X: Int, Y: Int) extends Filter[GreyScalePixel] {
  override def apply(grid: PixelGrid[GreyScalePixel]): PixelGrid[GreyScalePixel] = {
    if (Y <= X) return grid

    var result = Seq.empty[Seq[GreyScalePixel]]

    for (y <- grid.image) {
      var buffer_w = Seq.empty[GreyScalePixel]
      for (x <- y) {
        for (_ <- 0 until Y) buffer_w = buffer_w :+ x
      }
      for (_ <- 0 until X) result = result :+ buffer_w
    }
    val scaler = new Scaler(1.0 / Y)
    scaler.apply(new PixelGrid[GreyScalePixel](result))

//    new PixelGrid[GreyScalePixel](result)
  }

}
