package Processors.GridProcessors.GreyScaleFilters

import Models.Pixel.GreyScalePixel
import Models.PixelGrid
import Processors.GridProcessors.{Filter, GridCaster}
import Processors.PixelProcessors.Casters.GreyScaleBrightness

/*

    The brightness filter class.
    This filter accepts only GreyScale pixels
    according to the task

    Constructor accepts filter parameters.
    This way class may be reused for the potential
    multiple-image input

 */

class BrightnessFilter(value: Int) extends Filter[GreyScalePixel] {
  override def apply(grid: PixelGrid[GreyScalePixel]): PixelGrid[GreyScalePixel] = {
    val brightnessPixelCaster = new GreyScaleBrightness(value)
    val brightnessGridCaster = new GridCaster[GreyScalePixel, GreyScalePixel](brightnessPixelCaster)
    brightnessGridCaster.cast(grid)
  }
}
