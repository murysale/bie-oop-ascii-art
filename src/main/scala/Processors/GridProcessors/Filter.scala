package Processors.GridProcessors

import Models.Pixel.Pixel
import Models.PixelGrid

trait Filter[T <: Pixel] {
  def apply(grid: PixelGrid[T]): PixelGrid[T]
}
