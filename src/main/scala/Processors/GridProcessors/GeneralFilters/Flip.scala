package Processors.GridProcessors.GeneralFilters

import Models.Pixel.Pixel
import Models.PixelGrid
import Processors.GridProcessors.Filter

/*

    Filter which flips a PixelGrid
    containing any type of Pixels

    Constructor accepts filter parameters.
    This way class may be reused for the potential
    multiple-image input

 */

class Flip[T <: Pixel](flip_x: Boolean, flip_y: Boolean) extends Filter[T]  {
  override def apply(grid: PixelGrid[T]): PixelGrid[T] = {
    var result = grid
    if (flip_x) result = flip_horizontally(result)
    if (flip_y) result = flip_vertically(result)
    result
  }

  //flips grid vertically
  private def flip_vertically(grid: PixelGrid[T]): PixelGrid[T] = {
    new PixelGrid[T](grid.image.reverse)
  }

  //flips grid horizontally
  private def flip_horizontally(grid: PixelGrid[T]): PixelGrid[T] = {
    var result = Seq.empty[Seq[T]]
    for (y <- grid.image) result = result :+ y.reverse
    new PixelGrid[T](result)
  }
}
