package Processors.GridProcessors

import Models.Pixel.Pixel
import Models.PixelGrid
import Processors.PixelProcessors.PixelCaster

/*
    General Interface for the Grid
    casting
 */

class GridCaster[T <: Pixel, Y <: Pixel](caster: PixelCaster[T,Y]) {
  def cast(grid: PixelGrid[T]): PixelGrid[Y] = {
    var result = Seq.empty[Seq[Y]]
    for (y <- grid.image) {
      var buffer_row = Seq.empty[Y]
      for (x <- y) {
        buffer_row = buffer_row :+ caster.cast(x)
      }
      result = result :+ buffer_row
    }
    new PixelGrid[Y](result)
  }
}
