package MediaLoaders.ImageLoaders

import Models.Pixel.RGBAPixel
import Models.PixelGrid
import LocalImageLoaders.{LocalBMPImageLoader, LocalGIFImageLoader, LocalJPGImageLoader, LocalPNGImageLoader}

// General interface through which local images are imported
class   LocalImageLoader(path: String) extends ImageLoader[RGBAPixel] {

  // This function validates the input format and loads the image
  override def load(): PixelGrid[RGBAPixel] = {
    val splitInput = path.split('.')
    val inputFormat = splitInput(splitInput.length - 1)

    inputFormat.toLowerCase() match {
      case "png" =>
        new LocalPNGImageLoader(path).load()
      case "gif" =>
        new LocalGIFImageLoader(path).load()
      case "jpg" =>
        new LocalJPGImageLoader(path).load()
      case "bmp" =>
        new LocalBMPImageLoader(path).load()
      case _ =>
        throw new IllegalArgumentException(s"Image format $inputFormat is not supported!")
    }
  }
}
