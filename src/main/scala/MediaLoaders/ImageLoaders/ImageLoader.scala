package MediaLoaders.ImageLoaders

import MediaLoaders.MediaLoader
import Models.PixelGrid
import Models.Pixel.Pixel

/*

    General image loader interface

 */

trait ImageLoader[T <: Pixel] extends MediaLoader[PixelGrid[T]]{}
