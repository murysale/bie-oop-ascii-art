package MediaLoaders.ImageLoaders

import MediaLoaders.MediaLoader
import Models.Pixel.{Pixel, RGBAPixel}
import Models.PixelGrid

import java.awt.Color

/*

    Generates a random image with a ratio
    in a range from 10 to 100 pixels

 */

class RandomImageLoader extends ImageLoader[RGBAPixel]{
  private val lower_bound = 10
  private val upper_bound = 100

  override def load(): PixelGrid[RGBAPixel] = {
    val rand = new scala.util.Random
    val height = rand.between(lower_bound, upper_bound)
    val width = rand.between(lower_bound, upper_bound)
    var buffer_grid = Seq.empty[Seq[RGBAPixel]]

    for (x <- 0 until height) {
      var buffer_row = Seq.empty[RGBAPixel]
      for (y <- 0 until width) {
        buffer_row = buffer_row :+ RGBAPixel(rand.nextInt())
      }
      buffer_grid = buffer_grid :+ buffer_row
    }

    new PixelGrid[RGBAPixel](buffer_grid)
  }
}
