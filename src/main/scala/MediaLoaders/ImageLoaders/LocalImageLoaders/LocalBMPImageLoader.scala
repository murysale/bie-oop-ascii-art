package MediaLoaders.ImageLoaders.LocalImageLoaders

import Models.Pixel.RGBAPixel
import Models.PixelGrid

// Class importing BMP images
class LocalBMPImageLoader(path: String) {
  def load(): PixelGrid[RGBAPixel] = {
    val splitInput = path.split('.')
    val inputFormat = splitInput(splitInput.length - 1)

    if (inputFormat.toLowerCase() != "bmp")
      throw new IllegalArgumentException("Wrong image format's provided!")

    new ImageIOImageLoader(path).load()
  }
}
