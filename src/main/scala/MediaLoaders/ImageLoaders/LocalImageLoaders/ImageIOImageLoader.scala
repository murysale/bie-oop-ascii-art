package MediaLoaders.ImageLoaders.LocalImageLoaders

import MediaLoaders.ImageLoaders.ImageLoader
import Models.Pixel.RGBAPixel
import Models.PixelGrid

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

// That's the wrap around Java's ImageIO to import images as sequences of RGBAPixels
class ImageIOImageLoader(path: String) extends ImageLoader[RGBAPixel] {
  override def load(): PixelGrid[RGBAPixel] = {
    try {
      val image = ImageIO.read(new File(path))
      new PixelGrid[RGBAPixel](prepareImage(image))
    } catch {
      case _: Throwable => throw new Exception("Error while reading the image file!")
    }
  }

  //This function transforms Java's BufferedImage into a 2D Seq if Pixels
  private def prepareImage(image: BufferedImage): Seq[Seq[RGBAPixel]] = {
    if (image == null)
      throw new NullPointerException("Error: image is null!")

    var buffer_grid = Seq.empty[Seq[RGBAPixel]]

    val imgHeight = image.getHeight()
    val imgWidth = image.getWidth()


    for (y <- 0 until imgHeight) {
      var buffer_row = Seq.empty[RGBAPixel]
      for (x <- 0 until imgWidth) {

        buffer_row = buffer_row :+ RGBAPixel(image.getRGB(x, y))
      }
      buffer_grid = buffer_grid :+ buffer_row
    }

    buffer_grid
  }
}
