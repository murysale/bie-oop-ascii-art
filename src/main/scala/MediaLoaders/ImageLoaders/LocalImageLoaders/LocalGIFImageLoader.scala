package MediaLoaders.ImageLoaders.LocalImageLoaders

import Models.Pixel.RGBAPixel
import Models.PixelGrid

// Class importing GIF images
class LocalGIFImageLoader(path: String) {
  def load(): PixelGrid[RGBAPixel] = {
    val splitInput = path.split('.')
    val inputFormat = splitInput(splitInput.length - 1)

    if (inputFormat.toLowerCase() != "gif")
      throw new IllegalArgumentException("Wrong image format's provided!")

    new ImageIOImageLoader(path).load()
  }
}
