package MediaLoaders

/*
    Interface for possible extension.
    For example, a video source may be used in the future
 */

trait MediaLoader[T] {
  def load():T
}
