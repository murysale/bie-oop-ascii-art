package Exporters

import Models.Pixel.ASCIIPixel
import Models.PixelGrid
import java.nio.file.{Paths, Files}
import java.nio.charset.StandardCharsets

// Exports ASCII Pixels as a text file
class FileExporter(path: String) extends Exporter {
  override def `export`(value: PixelGrid[ASCIIPixel]): Unit = {
    Files.write(Paths.get(path), preparePixels(value).getBytes(StandardCharsets.UTF_8))
  }
}
