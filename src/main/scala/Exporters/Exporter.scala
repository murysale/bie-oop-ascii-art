package Exporters

import Models.Pixel.ASCIIPixel
import Models.PixelGrid

trait Exporter {
  def export(value: PixelGrid[ASCIIPixel]): Unit

  protected def preparePixels(value: PixelGrid[ASCIIPixel]): String = {
    var result = ""
    for (x <- value.image) {
      for (y <- x) result += y.value
      result += '\n'
    }
    result
  }
}
