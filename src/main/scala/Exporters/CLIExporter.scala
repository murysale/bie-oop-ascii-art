package Exporters

import Models.Pixel.ASCIIPixel
import Models.PixelGrid

// Exports ASCII pixels as text
class CLIExporter() extends Exporters.Exporter {
  override def `export`(value: PixelGrid[ASCIIPixel]): Unit = println(preparePixels(value))
}
