import UI.ConsoleUI
object Main extends App {
  val cui = new ConsoleUI()
  try {
    cui.start(args).execute
  } catch {
      case ex: NullPointerException => println(s"Null Pointer Exception: ${ex.getMessage}")
      case ex: IllegalArgumentException => println(s"Illegal Argument Exception: ${ex.getMessage}")
      case ex: Exception => println(s"Exception: ${ex.getMessage}")
      case _: Any => println("Unknown error's occurred!!")
  }

}