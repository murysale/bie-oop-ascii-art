package UI

import Executors.{CLIExecutor, Executor}
import MediaLoaders.ImageLoaders.{ImageLoader, LocalImageLoader, RandomImageLoader}
import Models.Pixel.{GreyScalePixel, RGBAPixel}
import Exporters.{CLIExporter, Exporter, FileExporter}
import Models.PixelGrid
import Processors.GridProcessors.Filter
import Processors.GridProcessors.GeneralFilters.Flip
import Processors.GridProcessors.GreyScaleFilters.{BrightnessFilter, FontAspectRatio, Invert, Scaler}


/*
    Console User Interface implementation.
    Accepts arguments from the CLI, parses them,
    and triggers the execution logic

 */

class ConsoleUI extends UI {

  // activates the interface and triggers the execution logic
  override def start(params: Seq[String]): Executor = {
    if (params.isEmpty)
      throw new IllegalArgumentException("No arguments provided!")

    val parsedArguments = parseArguments(params)

    println(parsedArguments)

    var importCmd = ("", "")
    var selectedTable = " .:-=+*#%@"
    var filters = Seq.empty[(String, String)]
    var importParsed, scaleParsed, far, flipx, flipy, invert, brightness, table, oconsole = false
    var exporters = Seq.empty[Exporter]

    for (arg <- parsedArguments) {
      if (arg._1.startsWith("--image") || arg._1.startsWith("--random-image")) {
        if (importParsed) throw new IllegalArgumentException("Error: Too many image sources!")
        importCmd = arg
        importParsed = true
      }
      else if (arg._1 == "--scale") {
        if (scaleParsed) throw new IllegalArgumentException("Error: Too many scale filters!")
        filters = filters :+ arg
        scaleParsed = true
      }
      else if (arg._1 == "--font-aspect-ratio") {
        if (far) throw new IllegalArgumentException("Error: Too many --font-aspect-ratio filters!")
        filters = filters :+ arg
        far = true
      }
      else if (arg._1 == "--flip") {
        if (arg._2 == "x") {
          if (flipx) throw new IllegalArgumentException("Error: Too many --flip x filters!")
          flipx = true
        }
        else if (arg._2 == "y") {
          if (flipy) throw new IllegalArgumentException("Error: Too many --flip x filters!")
          flipy = true
        }
        else throw new IllegalArgumentException("Error: Invalid --flip argument!")
      }
      else if (arg._1 == "--invert") {
        if (invert) throw new IllegalArgumentException("Error: Too many --invert filters!")
        invert = true
        filters = filters :+ arg
      }
      else if (arg._1 == "--brightness") {
        if (invert) throw new IllegalArgumentException("Error: Too many --brightness filters!")
        brightness = true
        filters = filters :+ arg
      }
      else if (arg._1 == "--table") {
        if (table) throw new IllegalArgumentException("Error: Too many --table filters!")
        table = true
        selectedTable = PREDEFINED_TABLES(arg._2)
      }
      else if (arg._1 == "--custom-table") {
        if (table) throw new IllegalArgumentException("Error: Too many --table filters!")
        table = true
        selectedTable = arg._2
      }
      else if (arg._1 == "--output-console") {
        if (oconsole) throw new IllegalArgumentException("Error: Too many --output-console filters!")
        oconsole = true
        exporters = exporters :+ new CLIExporter()
      }
      else if (arg._1 == "--output-file") {
        exporters = exporters :+ new FileExporter(arg._2)
      }
    }

    if (flipy || flipx) {
      var arg = ""
      if (flipx) arg += "x"
      if (flipy) arg += "y"
      filters = filters :+ ("--flip", arg)
    }

    if (exporters.isEmpty) exporters = exporters :+ new CLIExporter

    val sourceImage: PixelGrid[RGBAPixel] = if (importCmd._1 == "--image")
      new LocalImageLoader(importCmd._2).load()
      else new RandomImageLoader().load()


    val exe = new CLIExecutor(
      sourceImage,
      selectedTable,
      prepareFilters(filters),
      exporters
    )
    exe
  }

  // creates tuples (Command, Argument) or (Command, "") if no argument's provided
  private def parseArguments(args: Seq[String]): Seq[(String, String)] = {
    var result = Seq.empty[(String, String)]
    var buffer_cmd = ""
    for (arg <- args) {
      if (arg.startsWith("--")) {
        if (buffer_cmd != "")
          result = result :+ (buffer_cmd, "")
        buffer_cmd = arg
      }
      else {
        result = result :+ (buffer_cmd, arg)
        buffer_cmd = ""
      }
    }
    if (buffer_cmd != "") result = result :+ (buffer_cmd, "")
    result
  }

  //prepares filters to be pushed to the execution logic
  private def prepareFilters(filters: Seq[(String, String)]): Seq[Filter[GreyScalePixel]] = {
    var result = Seq.empty[Filter[GreyScalePixel]]
    for (filter <- filters) {
      result = result :+ (filter._1 match {
        case "--scale" =>
          new Scaler(filter._2.toDouble)

        case "--font-aspect-ratio" =>
          val args = filter._2.split(':')
          new FontAspectRatio(args(0).toInt, args(1).toInt)

        case "--flip" =>
          var flipx, flipy = false
          if (filter._2.contains('x')) flipx = true
          if (filter._2.contains('y')) flipy = true
          new Flip[GreyScalePixel](flipx, flipy)

        case "--invert" =>
          new Invert

        case "--brightness" =>
          new BrightnessFilter(filter._2.toInt)
      })
    }
    result
  }

}
