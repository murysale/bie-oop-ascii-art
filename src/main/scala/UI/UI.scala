package UI

//Interface for  the UI. Extensions such as GUI are possible

trait UI {
  //I store tables here, since the chosen table is the result of parsing of the input
  protected val PREDEFINED_TABLES: Map[String, String] = Map(
    "default" -> " .:-=+*#%@",
    "extended" -> "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ",
    "sharp" -> "@$#%&*,.",
    "non-linear" -> "@$#%&&&&&&&&&****,,,,,,,................................."
  )

  def start(params: Seq[String]): Executors.Executor
}
