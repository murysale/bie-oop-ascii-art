ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "ASCII_Art",
    libraryDependencies += "org.scalatest" % "scalatest_2.13" % "3.2.14" % Test,
    libraryDependencies += "org.mockito" % "mockito-scala_2.13" % "1.17.12" % Test
  )
